#!/bin/bash

REMOTE_ADDR=169.254.1.1
REMOTE_DIR=/mnt/linux
MOUNT_DIR=/mnt/nfs

mkdir -p ${MOUNT_DIR}
mount -o nolock ${REMOTE_ADDR}:${REMOTE_DIR} ${MOUNT_DIR}
