#!/bin/bash

test -r /etc/default/network && source /etc/default/network
[ -z $NETWORK_MANAGER ] && NETWORK_MANAGER=ifupdown
source /usr/share/colibri/helpers/network/${NETWORK_MANAGER}_nm_functions.sh

get_internet_state
