INSTALL	?= install
MKDIR	?= mkdir
CP		?= cp
CHMOD	?= chmod

DESTDIR ?= /

all:

install-network-helpers:
	$(MKDIR) -p $(DESTDIR)/usr/share/colibri/helpers/network
	$(CP) -aRv network/* $(DESTDIR)/usr/share/colibri/helpers/network/

install-development-helpers:
	$(MKDIR) -p $(DESTDIR)/usr/share/colibri/helpers/development
	$(CP) -aRv development/* $(DESTDIR)/usr/share/colibri/helpers/development/

install: install-network-helpers install-development-helpers
	@echo
	@echo "* Colibri helper scripts installed to $(DESTDIR)"
